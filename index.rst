.. TestSphinx documentation master file, created by
   sphinx-quickstart on Mon Apr  9 13:29:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to The documentation!
======================================
Search the `Docs`_,
Consult a `Cheat Sheet`_,
Or use an `Example`_.

.. _Docs: http://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html
.. _Cheat Sheet: https://matplotlib.org/sampledoc/cheatsheet.html
.. _Example: https://pythonhosted.org/an_example_pypi_project/sphinx.html#full-code-example

.. automodule:: main
    :members:

The Taps i do
=============
.. automodule:: Taps.tap01
    :members:

