"""
Tap 01
------
"""
def tap01(name):
    """
    :param name: The Name to Print
    :return: 1 - Success | 0 - Fail
    """
    print(name)
    return 1