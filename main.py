"""
**********************************
The Mandatory Hello World Program
**********************************
"""

def hello_world():
    """
    Prints Hello World!::
    print("Hello World")
    """
    print("Hello World")


if __name__ == "__main__":
    """
    Runs our program.
    Also known as the main function.
    """
    hello_world()